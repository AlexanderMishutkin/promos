from django.db import models


class Prize(models.Model):
    description = models.CharField(null=False, max_length=2000)


class Participant(models.Model):
    name = models.CharField(null=False, max_length=200)


class Result(models.Model):
    winner = models.ForeignKey(Participant, null=False, on_delete=models.CASCADE)
    prize = models.ForeignKey(Prize, null=False, on_delete=models.CASCADE)


class Promo(models.Model):
    """
    Промоакция
    """
    name = models.CharField(null=False, max_length=200)
    description = models.CharField(null=True, max_length=2000)
    prizes = models.ManyToManyField(Prize)
    participants = models.ManyToManyField(Participant)

