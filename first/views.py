from django.shortcuts import render
import django.http
from django.http import JsonResponse
from django.core import serializers
from django.forms.models import model_to_dict
from django.http import HttpResponse, HttpResponseBadRequest
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from first.models import Promo, Prize, Participant, Result
import json
import datetime


def promo_to_dict(instance):
    dictionary = model_to_dict(instance)
    dictionary['participants'] = [model_to_dict(a) for a in dictionary['participants']]
    dictionary['prizes'] = [model_to_dict(a) for a in dictionary['prizes']]
    return dictionary


@api_view(["POST", "GET"])
@permission_classes((AllowAny,))
def promo(request):
    """
    Добавление промоакции с возможностью указания названия (name), описания (description)

    Описание – не обязательный параметр, название – обязательный
    """

    if request.method == "POST":
        promo = Promo.objects.create(name=request.data.get("name"), description=request.data.get("description", None))
        promo.save()
        return django.http.HttpResponse('ok')
    else:
        promos = Promo.objects.all()
        answer = []
        for prom in promos:
            answer.append({'id': prom.id, 'name': prom.name, 'description': prom.description})
        return HttpResponse(json.dumps(answer), content_type='application/json')


@api_view(["PUT", "GET", "DELETE"])
@permission_classes((AllowAny,))
def promo_id(request, id):
    """
    Получение полной информации (с информацией об участниках и призах) о промоакции по идентификатору
    """

    if request.method == "PUT":
        promo = Promo.objects.get(id=id)
        if request.data.get("name"):
            promo.name = request.data.get("name")
            if request.data.get("description", -1) != -1:
                promo.description = request.data.get("description", None)
            promo.save()
            return django.http.HttpResponse('ok')
        return django.http.HttpResponseBadRequest('no name')
    elif request.method == "GET":
        return HttpResponse(json.dumps(promo_to_dict(Promo.objects.get(id=id))), content_type='application/json')
    elif request.method == "DELETE":
        promo = Promo.objects.get(id=id)
        promo.delete()
        return django.http.HttpResponse('ok')


@api_view(["POST"])
@permission_classes((AllowAny,))
def promo_participant(request, id):
    promo = Promo.objects.get(id=id)
    participant = Participant.objects.create(name=request.data.get("name"))
    promo.participants.add(participant)
    promo.save()
    participant.save()
    return django.http.HttpResponseBadRequest(participant.id)


@api_view(["DELETE"])
@permission_classes((AllowAny,))
def promo_participant_delete(request, promo_id, id):
    promo = Promo.objects.get(id=promo_id)
    participant = Participant.objects.get(id=id)
    promo.participants.remove(participant)
    promo.save()
    return django.http.HttpResponseBadRequest("ok")


@api_view(["POST"])
@permission_classes((AllowAny,))
def promo_prize(request, id):
    promo = Promo.objects.get(id=id)
    prize = Prize.objects.create(description=request.data.get("description"))
    promo.prizes.add(prize)
    promo.save()
    prize.save()
    return django.http.HttpResponseBadRequest(prize.id)


@api_view(["DELETE"])
@permission_classes((AllowAny,))
def promo_prize_delete(request, promo_id, id):
    promo = Promo.objects.get(id=promo_id)
    prize = Prize.objects.get(id=id)
    promo.prizes.remove(prize)
    promo.save()
    return django.http.HttpResponseBadRequest("ok")


@api_view(["GET"])
@permission_classes((AllowAny,))
def raffle(request, id):
    promo = Promo.objects.get(id=id)
    prizes = promo.prizes
    participants = promo.participants
    if participants.count() != prizes.count():
        response = django.http.HttpResponse("Conflict")
        response.status_code = 409
        return response
    answ = []
    for prize, participant in zip(prizes.all(), participants.all()):
        Result.objects.create(winner=participant, prize=prize)
        answ.append({"winner":model_to_dict(participant), "prize":model_to_dict(prize)})

    return HttpResponse(json.dumps(answ), content_type='application/json')

